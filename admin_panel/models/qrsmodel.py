from django.db import models

class QRmodel(models.Model):
    name = models.CharField(max_length=255)
    image = models.ImageField(upload_to='qr_image/')
    audio = models.FileField(upload_to='qr_audio/')
    text = models.TextField(default="")
    unique_id = models.CharField(max_length=255)
