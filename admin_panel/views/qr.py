from django.shortcuts import render,redirect,get_object_or_404
from ..models import *
from datetime import datetime

def QRPage(request,unique_id):
    qr = get_object_or_404(QRmodel,unique_id=unique_id)
    context = {
        'qr':qr,
    }
    return render(request,'admin_panel/qr-page.html',context)

def QRCodes(request):
    qrcodes = QRmodel.objects.all()

    context = {
        'qrcodes':qrcodes,
    }
    return render(request,'admin_panel/admin-qr-list.html',context)

def QRCodePageAdd(request):
    if request.method == 'POST':
        name = request.POST['page_name']
        text = request.POST['page_text']
        audio = request.FILES.get('page_audio')
        nowdate = str(datetime.now())
        uniqueurl = ''.join(e for e in nowdate if e.isalnum())
        QRmodel.objects.create(name=name,text=text,unique_id=uniqueurl,audio=audio)
        return redirect('qr-pages')
    context ={

    }
    return render(request,'admin_panel/admin-qr-add.html',context)

def QRCodeCreationKcite(request):
    context = {

    }
    return render(request,'admin_panel/qrcode-creation-kcite.html',context)