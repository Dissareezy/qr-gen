from django.db import models

class Tarif(models.Model):
    name = models.CharField(max_length=255)
    time_needed = models.CharField(max_length=255)
    price = models.CharField(max_length=255)