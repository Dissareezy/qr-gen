from django.db import models


class HeaderInfo(models.Model):
    workingdays = models.TextField(default='')
    workinghours = models.TextField(default='')