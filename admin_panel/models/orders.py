from django.db import models
from .cards import *
from .tarif import *
from datetime import datetime

class Order(models.Model):
    class StatusChoices(models.TextChoices):
        ACTIVE = 'ACTIVE', 'active'
        DELETED = 'DELETED', 'deleted'
        CANCELED = 'CANCELED', 'canceled'
        WAITING = 'WAITING', 'waiting'
    customer = models.ForeignKey(Card,
                                 on_delete=models.CASCADE,
                                 related_name = "customer",
                                 )
    tarif = models.ForeignKey(Tarif,
                              on_delete=models.CASCADE,
                              related_name= "tarif",
                              )
    employee = models.ForeignKey(Card,
                                 on_delete=models.CASCADE,
                                 related_name = "employee",
                                 blank=True,
                                 )
    request_date = models.DateTimeField()
    create_date = models.DateTimeField(default=datetime.now(),
                                        blank=True,
                                        )
    order_date = models.DateTimeField(blank=True)
    status = models.CharField(max_length=255,
                              choices=StatusChoices.choices,
                              default=StatusChoices.WAITING
                              )
    price = models.CharField(max_length=255)
    comments = models.TextField(default="")