from django.shortcuts import render,redirect,get_object_or_404
from admin_panel.models import *

def OrderAddView(request):
    tarifs = Tarif.objects.all()
    context = {
        'tarifs':tarifs,
    }
    if request.method == 'POST':
        type = request.POST['type']
        if type == 'add':
            customer = request.user
            tarif_pk = request.POST['tarif_pk']
            request_date = request.POST['request_date']
            price = request.POST['price']
            comments = request.POST['comments']
            tarif = get_object_or_404(Tarif,pk=tarif_pk)
            Order.objects.create(customer=customer,tarif=tarif,request_date=request_date,price=price,comments=comments)
            return redirect('panel')
        return redirect('panel')
    return render(request,'front/order-add.html',context)