from django.shortcuts import render, redirect, get_object_or_404 , HttpResponse
from ..models import *

def EmployeersView(request):
    employeers = Card.objects.filter(role="employee")
    context = {
        'employeers':employeers,
    }
    return render(request,'admin_panel/admin-employeers.html',context)

def EmployeeAddView(request):
    context ={

    }
    if request.method == 'POST':
        type = request.POST['type']
        if type == 'add':
            username = request.POST['username']
            first_name = request.POST['first_name']
            last_name = request.POST['last_name']
            password = request.POST['password']
            re_password = request.POST['re_password']
            if password == re_password:
                try:
                    user = User.objects.get(username=username)
                    context['error'] = 1
                    return render(request, 'admin_panel/admin-employee-add.html', context=context)
                except User.DoesNotExist:
                    user = User.objects.create_user(
                        username=username,
                        password=password,
                        first_name=first_name,
                        last_name=last_name,
                    )
                    user.save()
                    card = Card(owner=user)
                    card.role = card.RoleChoices.ADMINISTRATOR
                    card.save()
                    context['error'] = 0
                    return render(request, 'admin_panel/admin-employee-add.html', context=context)
            else:
                context['error'] = 2
                return render(request, 'admin_panel/admin-employee-add.html', context=context)
    else:
        pass
    return render(request, 'admin_panel/admin-employee-add.html', context=context)

def employeeEditView(request, employee_card_id):
    card = get_object_or_404(Card, pk=employee_card_id)
    user = card.owner
    raw_password = "XXsalamdias!XX"
    context = {
        'card': card,
        'raw_password': raw_password,
    }
    if request.method == 'POST':
        type = request.POST['form']
        if type == 'employee-edit':
            username = request.POST['username']
            first_name = request.POST['first_name']
            last_name = request.POST['last_name']
            email = request.POST['email']
            password = request.POST['password']
            re_password = request.POST['re_password']
            status = request.POST['status']
            if password == re_password:
                if user.username != username and User.objects.filter(username='username').count():
                    context['error'] = 1
                    return render(request, 'admin_panel/admin-employee-add.html', context=context)
                else:
                    user.username = username
                    if password != raw_password:
                        user.set_password(password)
                    user.email = email
                    user.first_name = first_name
                    user.last_name = last_name
                    user.save()
                    if status == '0':
                        card.status = Card.StatusChoices.ACTIVE
                    elif status == '1':
                        card.status = Card.StatusChoices.DELETED
                    elif status == '2':
                        card.status = Card.StatusChoices.BANNED
                    else:
                        card.status = Card.StatusChoices.HOLIDAY
                    card.save()
                    context['error'] = 0
                    return render(request, 'admin_panel/admin-employee-add.html', context=context)
            else:
                context['error'] = 2
            return render(request, 'admin_panel/admin-employee-add.html', context=context)
        else:
            pass
        if type == 'delete':
            card.delete()
            print('deleted')
            return redirect('employees')
    return render(request, 'admin_panel/admin-employee-add.html', context=context)
