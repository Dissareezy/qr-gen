from django.shortcuts import render, redirect, get_object_or_404 , HttpResponse
from ..models import *

def TarifView(request):
    tarifs = Tarif.objects.all()
    context = {
        'tarifs':tarifs,
    }
    return render(request,'admin_panel/admin-tarif.html',context)

def TarifAddView(request):
    if request.method == 'POST':
        type = request.POST['type']
        if type == 'add':
            name = request.POST['name']
            time_needed = request.POST['time_needed']
            price = request.POST['price']
            Tarif.objects.create(name=name,time_needed=time_needed,price=price)
            return redirect('panel')
    return redirect('panel')

def TarifEditView(request,tarif_pk):
    tarif = get_object_or_404(Tarif,pk = tarif_pk)

    if request.method == 'POST':
        type = request.POST['type']
        if type == 'edit':
            name = request.POST['name']
            time_needed = request.POST['time_needed']
            price = request.POST['price']
            tarif.name = name
            tarif.time_needed = time_needed
            tarif.price = price
            tarif.save()
            return redirect('panel')
        if type == 'delete':
            tarif.delete()
            return redirect('panel')

