from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Card(models.Model):
    class RoleChoices(models.TextChoices):
        COMPANY_OWNER = 'COMPANY_OWNER', 'company owner'
        ADMINISTRATOR = 'ADMINISTRATOR', 'administrator'
        EMPLOYEE = 'EMPLOYEE', 'employee'
        USER = 'USER', 'user'

    class StatusChoices(models.TextChoices):
        ACTIVE = 'ACTIVE', 'active'
        DELETED = 'DELETED', 'deleted'
        BANNED = 'BANNED', 'banned'
        HOLIDAY = 'HOLIDAY', 'holiday'
    name = models.CharField(max_length=255)
    owner = models.OneToOneField(User,
                                 on_delete=models.CASCADE,
                                 related_name="card"
                                 )
    role = models.CharField(max_length=255,
                            choices=RoleChoices.choices,
                            default=RoleChoices.USER,
                            )
    status = models.CharField(max_length=255,
                              choices=StatusChoices.choices,
                              default=StatusChoices.ACTIVE
                              )
    phone = models.CharField(max_length=255)
