from django.shortcuts import render, redirect, get_object_or_404 , HttpResponse
from ..models import *

def OrdersView(request):
    orders = Order.objects.all()
    context = {
        'orders':orders,
    }
    return render(request,'admin_panel/admin-orders.html',context)

def OrderEdit(request,order_pk):
    order = get_object_or_404(Order,pk=order_pk)
    context  = {
        'order':order,
    }
    if request.method == 'POST':
        type = request.POST['type']
        if type == 'edit':
            employee_pk = request.POST['employee_pk']
            tarif_pk = request.POST['tarif_pk']
            order_date = request.POST['order_date']
            status = request.POST['status']
            price = request.POST['price']
            employee = get_object_or_404(Card,pk=employee_pk)
            tarif = get_object_or_404(Tarif,pk=tarif_pk)
            order.employee = employee
            order.tarif = tarif
            order.order_date = order_date
            order.status = status
            order.price = price
            order.save()
            return redirect('panel')
        if type == 'delete':
            order.status = order.StatusChoices.DELETED
            order.save()
            return redirect('panel')
        return redirect('panel')
    return render(request,'admin_panel/admin-order-edit.html',context)