from django.urls import path
from .views import *


urlpatterns = [
    path('', adminPanelView, name='panel'),

    path('qr-page-create/', QRCodePageAdd,name='qr-page-add'),
    path('qr-generate/',QRCodeCreationKcite,name='qr-generate'),
    path('qr-page-list/',QRCodes,name='qr-pages'),

    path('qr-page/<str:unique_id>',QRPage,name='qr-page'),
    ]